# OMBU Default Theme

## Setup

Run `npm install` in theme root to install dev libraries

Run `npm start` in theme root to start gulp watching for file changes, compiles css and js

## CSS Organization

CSS is organizaed using SMACSS conventions (https://smacss.com/)

### base

#### base.scss

Base styles for HTML element types, such as `img`, `h1`, `body`, no custom HTML code is included, such as classes or IDs

#### reset.scss

Resets and normalization

### init

#### bootstrap-custom.scss

Custom bootstrap import, including only the essential bootstrap components and libraries

#### contants.scss

Global theme variables such as colors, fonts, and breakpoints

#### fonts.scss

Font-face declarations for local custom fonts

#### mixins.scss

Global functions and reusable code blocks

### layout

#### layout.scss

Global layout properties

### modules

Component-level styles, organized by type of component. The bulk of the project's CSS lives in this folder.

### init.scss

Compiles init styles (bootstrap, contstants, fonts, and mixins), can be imported by site stylesheets

### style.scss

Compiles base styles and component-level styles


## External Libraries

External Libraries (such as the example "Flickity") should be managed through npm wherever possible. See `gulpfile.js` and `style.scss` for an example on including external CSS and JS in the theme.

## Standard Conventions

Class names should use the BEM (Block, Element, Modifier) naming conventions (http://getbem.com/) wherever possible, such as `example__title`, `example__description` and `example__title--prominent`. CSS should be kept as "flat" as possible without nesting classes, using only relevant class names and IDs to style elements, not element types such as `h2`. All modifiers, such as media queries and contextual styling (such as when a component is in a section with a dark background), should be grouped with the applicable component.

## Example component

The included example component has HTML, CSS, and JS associated with it. It uses the same component name `example` in all contexts.